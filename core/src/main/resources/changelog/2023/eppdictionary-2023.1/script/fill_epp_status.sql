INSERT INTO epp_status (id, status)
VALUES
    (-1, 'DONE'), -- конечный статус
    (-2, 'REJECTED'), -- конечный статус
    (1, 'CREATION'),
    (2, 'PUBLISHED'),
    (3, 'REVIEW'),
    (4, 'REWORK');
