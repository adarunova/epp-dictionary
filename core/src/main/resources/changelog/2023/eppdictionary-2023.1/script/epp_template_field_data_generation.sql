CREATE OR REPLACE PROCEDURE insert_epp_template_field_student(
    role_param IN epp_template_field.role_id%TYPE,
    status_param IN epp_template_field.status_id%TYPE)
    LANGUAGE plpgsql AS
$$
DECLARE
    access epp_template_field.access%TYPE := 'VIEW';
    deny_access epp_template_field.access%TYPE := 'DENY';
BEGIN
    IF (status_param = 1 OR status_param = 4) THEN
        access := 'EDIT';
    END IF;
    IF (role_param = 3 OR role_param = 4) THEN
        deny_access := 'VIEW';
    END IF;
    INSERT INTO epp_template_field (template_id, field_id, status_id, role_id, access, ordinal, is_required)
    VALUES (1, 1, status_param, role_param, access, 1, true),
           (1, 2, status_param, role_param, access, 2, true),
           (1, 8, status_param, role_param, access, 3, true),
           (1, 9, status_param, role_param, access, 4, true),
           (1, 3, status_param, role_param, access, 5, false),
           (1, 7, status_param, role_param, access, 6, true),
           (1, 4, status_param, role_param, access, 7, false),
           (1, 5, status_param, role_param, access, 8, false),
           (1, 34, status_param, role_param, deny_access, 9, true),
           (1, 33, status_param, role_param, deny_access, 10, true),
           (1, 35, status_param, role_param, deny_access, 11, true),
           (1, 36, status_param, role_param, deny_access, 12, true),
           (1, 31, status_param, role_param, deny_access, 13, true),
           (1, 40, status_param, role_param, deny_access, 14, true),
           (1, 39, status_param, role_param, deny_access, 15, true),
           (1, 44, status_param, role_param, deny_access, 16, true),
           (1, 38, status_param, role_param, deny_access, 17, true),
           (1, 42, status_param, role_param, deny_access, 18, true),
           (1, 43, status_param, role_param, deny_access, 19, true),
           (1, 37, status_param, role_param, deny_access, 20, true),
           (1, 32, status_param, role_param, deny_access, 21, true),

           (2, 10, status_param, role_param, access, 1, true),
           (2, 1, status_param, role_param, access, 2, true),
           (2, 2, status_param, role_param, access, 3, true),
           (2, 4, status_param, role_param, access, 4, false),
           (2, 5, status_param, role_param, access, 5, false),
           (2, 34, status_param, role_param, deny_access, 6, true),
           (2, 33, status_param, role_param, deny_access, 7, true),
           (2, 35, status_param, role_param, deny_access, 8, true),
           (2, 36, status_param, role_param, deny_access, 9, true),
           (2, 31, status_param, role_param, deny_access, 10, true),
           (2, 40, status_param, role_param, deny_access, 11, true),
           (2, 39, status_param, role_param, deny_access, 12, true),
           (2, 44, status_param, role_param, deny_access, 13, true),
           (2, 38, status_param, role_param, deny_access, 14, true),
           (2, 42, status_param, role_param, deny_access, 15, true),
           (2, 43, status_param, role_param, deny_access, 16, true),
           (2, 37, status_param, role_param, deny_access, 17, true),
           (2, 32, status_param, role_param, deny_access, 18, true),

           (3, 1, status_param, role_param, access, 1, true),
           (3, 2, status_param, role_param, access, 2, true),
           (3, 3, status_param, role_param, access, 3, false),
           (3, 4, status_param, role_param, access, 4, false),
           (3, 5, status_param, role_param, access, 5, false),
           (3, 34, status_param, role_param, deny_access, 6, true),
           (3, 33, status_param, role_param, deny_access, 7, true),
           (3, 35, status_param, role_param, deny_access, 8, true),
           (3, 36, status_param, role_param, deny_access, 9, true),
           (3, 31, status_param, role_param, deny_access, 10, true),
           (3, 40, status_param, role_param, deny_access, 11, true),
           (3, 39, status_param, role_param, deny_access, 12, true),
           (3, 44, status_param, role_param, deny_access, 13, true),
           (3, 38, status_param, role_param, deny_access, 14, true),
           (3, 42, status_param, role_param, deny_access, 15, true),
           (3, 43, status_param, role_param, deny_access, 16, true),
           (3, 37, status_param, role_param, deny_access, 17, true),
           (3, 32, status_param, role_param, deny_access, 18, true),

           (4, 1, status_param, role_param, access, 1, true),
           (4, 2, status_param, role_param, access, 2, true),
           (4, 3, status_param, role_param, access, 3, false),
           (4, 4, status_param, role_param, access, 4, false),
           (4, 5, status_param, role_param, access, 5, false),
           (4, 34, status_param, role_param, deny_access, 6, true),
           (4, 33, status_param, role_param, deny_access, 7, true),
           (4, 35, status_param, role_param, deny_access, 8, true),
           (4, 36, status_param, role_param, deny_access, 9, true),
           (4, 31, status_param, role_param, deny_access, 10, true),
           (4, 40, status_param, role_param, deny_access, 11, true),
           (4, 39, status_param, role_param, deny_access, 12, true),
           (4, 44, status_param, role_param, deny_access, 13, true),
           (4, 38, status_param, role_param, deny_access, 14, true),
           (4, 42, status_param, role_param, deny_access, 15, true),
           (4, 43, status_param, role_param, deny_access, 16, true),
           (4, 37, status_param, role_param, deny_access, 17, true),
           (4, 32, status_param, role_param, deny_access, 18, true),

           (5, 1, status_param, role_param, access, 1, true),
           (5, 2, status_param, role_param, access, 2, true),
           (5, 3, status_param, role_param, access, 3, false),
           (5, 4, status_param, role_param, access, 4, false),
           (5, 5, status_param, role_param, access, 5, false),
           (5, 34, status_param, role_param, deny_access, 6, true),
           (5, 33, status_param, role_param, deny_access, 7, true),
           (5, 35, status_param, role_param, deny_access, 8, true),
           (5, 36, status_param, role_param, deny_access, 9, true),
           (5, 31, status_param, role_param, deny_access, 10, true),
           (5, 40, status_param, role_param, deny_access, 11, true),
           (5, 39, status_param, role_param, deny_access, 12, true),
           (5, 44, status_param, role_param, deny_access, 13, true),
           (5, 38, status_param, role_param, deny_access, 14, true),
           (5, 42, status_param, role_param, deny_access, 15, true),
           (5, 43, status_param, role_param, deny_access, 16, true),
           (5, 37, status_param, role_param, deny_access, 17, true),
           (5, 32, status_param, role_param, deny_access, 18, true),

           (6, 2, status_param, role_param, access, 1, true),
           (6, 1, status_param, role_param, access, 2, true),
           (6, 3, status_param, role_param, access, 3, false),
           (6, 11, status_param, role_param, access, 4, true),
           (6, 12, status_param, role_param, access, 5, true),
           (6, 13, status_param, role_param, access, 6, true),
           (6, 14, status_param, role_param, access, 7, true),
           (6, 4, status_param, role_param, access, 8, false),
           (6, 5, status_param, role_param, access, 9, false),
           (6, 34, status_param, role_param, deny_access, 10, true),
           (6, 33, status_param, role_param, deny_access, 11, true),
           (6, 35, status_param, role_param, deny_access, 12, true),
           (6, 36, status_param, role_param, deny_access, 13, true),
           (6, 31, status_param, role_param, deny_access, 14, true),
           (6, 40, status_param, role_param, deny_access, 15, true),
           (6, 39, status_param, role_param, deny_access, 16, true),
           (6, 44, status_param, role_param, deny_access, 17, true),
           (6, 38, status_param, role_param, deny_access, 18, true),
           (6, 42, status_param, role_param, deny_access, 19, true),
           (6, 43, status_param, role_param, deny_access, 20, true),
           (6, 37, status_param, role_param, deny_access, 21, true),
           (6, 32, status_param, role_param, deny_access, 22, true),

           (7, 1, status_param, role_param, access, 1, true),
           (7, 2, status_param, role_param, access, 2, true),
           (7, 3, status_param, role_param, access, 3, false),
           (7, 4, status_param, role_param, access, 4, false),
           (7, 5, status_param, role_param, access, 5, false),
           (7, 34, status_param, role_param, deny_access, 6, true),
           (7, 33, status_param, role_param, deny_access, 7, true),
           (7, 35, status_param, role_param, deny_access, 8, true),
           (7, 36, status_param, role_param, deny_access, 9, true),
           (7, 31, status_param, role_param, deny_access, 10, true),
           (7, 40, status_param, role_param, deny_access, 11, true),
           (7, 39, status_param, role_param, deny_access, 12, true),
           (7, 44, status_param, role_param, deny_access, 13, true),
           (7, 38, status_param, role_param, deny_access, 14, true),
           (7, 42, status_param, role_param, deny_access, 15, true),
           (7, 43, status_param, role_param, deny_access, 16, true),
           (7, 37, status_param, role_param, deny_access, 17, true),
           (7, 32, status_param, role_param, deny_access, 18, true),

           (8, 1, status_param, role_param, access, 1, true),
           (8, 2, status_param, role_param, access, 2, true),
           (8, 3, status_param, role_param, access, 3, false),
           (8, 4, status_param, role_param, access, 4, false),
           (8, 5, status_param, role_param, access, 5, false),
           (8, 34, status_param, role_param, deny_access, 6, true),
           (8, 33, status_param, role_param, deny_access, 7, true),
           (8, 35, status_param, role_param, deny_access, 8, true),
           (8, 36, status_param, role_param, deny_access, 9, true),
           (8, 31, status_param, role_param, deny_access, 10, true),
           (8, 40, status_param, role_param, deny_access, 11, true),
           (8, 39, status_param, role_param, deny_access, 12, true),
           (8, 44, status_param, role_param, deny_access, 13, true),
           (8, 38, status_param, role_param, deny_access, 14, true),
           (8, 42, status_param, role_param, deny_access, 15, true),
           (8, 43, status_param, role_param, deny_access, 16, true),
           (8, 37, status_param, role_param, deny_access, 17, true),
           (8, 32, status_param, role_param, deny_access, 18, true),

           (9, 2, status_param, role_param, access, 1, true),
           (9, 1, status_param, role_param, access, 2, true),
           (9, 15, status_param, role_param, access, 3, true),
           (9, 16, status_param, role_param, access, 4, true),
           (9, 17, status_param, role_param, access, 5, true),
           (9, 18, status_param, role_param, access, 6, true),
           (9, 19, status_param, role_param, access, 7, true),
           (9, 3, status_param, role_param, access, 8, false),
           (9, 34, status_param, role_param, deny_access, 6, true),
           (9, 33, status_param, role_param, deny_access, 7, true),
           (9, 35, status_param, role_param, deny_access, 8, true),
           (9, 36, status_param, role_param, deny_access, 9, true),
           (9, 31, status_param, role_param, deny_access, 10, true),
           (9, 40, status_param, role_param, deny_access, 11, true),
           (9, 39, status_param, role_param, deny_access, 12, true),
           (9, 44, status_param, role_param, deny_access, 13, true),
           (9, 38, status_param, role_param, deny_access, 14, true),
           (9, 42, status_param, role_param, deny_access, 15, true),
           (9, 43, status_param, role_param, deny_access, 16, true),
           (9, 37, status_param, role_param, deny_access, 17, true),
           (9, 32, status_param, role_param, deny_access, 18, true),

           (10, 2, status_param, role_param, access, 1, true),
           (10, 1, status_param, role_param, access, 2, true),
           (10, 46, status_param, role_param, access, 3, true),
           (10, 45, status_param, role_param, access, 4, true),
           (10, 4, status_param, role_param, access, 5, false),
           (10, 5, status_param, role_param, access, 6, false),
           (10, 34, status_param, role_param, deny_access, 7, true),
           (10, 33, status_param, role_param, deny_access, 8, true),
           (10, 35, status_param, role_param, deny_access, 9, true),
           (10, 36, status_param, role_param, deny_access, 10, true),
           (10, 31, status_param, role_param, deny_access, 11, true),
           (10, 40, status_param, role_param, deny_access, 12, true),
           (10, 39, status_param, role_param, deny_access, 13, true),
           (10, 44, status_param, role_param, deny_access, 14, true),
           (10, 38, status_param, role_param, deny_access, 15, true),
           (10, 42, status_param, role_param, deny_access, 16, true),
           (10, 43, status_param, role_param, deny_access, 17, true),
           (10, 37, status_param, role_param, deny_access, 18, true),
           (10, 32, status_param, role_param, deny_access, 19, true),

           (11, 2, status_param, role_param, access, 1, true),
           (11, 1, status_param, role_param, access, 2, true),
           (11, 30, status_param, role_param, access, 3, true),
           (11, 20, status_param, role_param, access, 4, true),
           (11, 4, status_param, role_param, access, 5, false),
           (11, 5, status_param, role_param, access, 6, false),
           (11, 34, status_param, role_param, deny_access, 7, true),
           (11, 33, status_param, role_param, deny_access, 8, true),
           (11, 35, status_param, role_param, deny_access, 9, true),
           (11, 36, status_param, role_param, deny_access, 10, true),
           (11, 31, status_param, role_param, deny_access, 11, true),
           (11, 40, status_param, role_param, deny_access, 12, true),
           (11, 39, status_param, role_param, deny_access, 13, true),
           (11, 44, status_param, role_param, deny_access, 14, true),
           (11, 38, status_param, role_param, deny_access, 15, true),
           (11, 42, status_param, role_param, deny_access, 16, true),
           (11, 43, status_param, role_param, deny_access, 17, true),
           (11, 37, status_param, role_param, deny_access, 18, true),
           (11, 32, status_param, role_param, deny_access, 19, true),

           (12, 1, status_param, role_param, access, 1, true),
           (12, 3, status_param, role_param, access, 2, false),
           (12, 34, status_param, role_param, deny_access, 3, true),
           (12, 33, status_param, role_param, deny_access, 4, true),
           (12, 35, status_param, role_param, deny_access, 5, true),
           (12, 36, status_param, role_param, deny_access, 6, true),
           (12, 31, status_param, role_param, deny_access, 7, true),
           (12, 40, status_param, role_param, deny_access, 8, true),
           (12, 39, status_param, role_param, deny_access, 9, true),
           (12, 44, status_param, role_param, deny_access, 10, true),
           (12, 38, status_param, role_param, deny_access, 11, true),
           (12, 42, status_param, role_param, deny_access, 12, true),
           (12, 43, status_param, role_param, deny_access, 13, true),
           (12, 37, status_param, role_param, deny_access, 14, true),
           (12, 32, status_param, role_param, deny_access, 15, true),

           (13, 1, status_param, role_param, access, 1, true),
           (13, 3, status_param, role_param, access, 2, false),
           (13, 4, status_param, role_param, access, 3, false),
           (13, 5, status_param, role_param, access, 4, false),
           (13, 34, status_param, role_param, deny_access, 5, true),
           (13, 33, status_param, role_param, deny_access, 6, true),
           (13, 35, status_param, role_param, deny_access, 7, true),
           (13, 36, status_param, role_param, deny_access, 8, true),
           (13, 31, status_param, role_param, deny_access, 9, true),
           (13, 40, status_param, role_param, deny_access, 10, true),
           (13, 39, status_param, role_param, deny_access, 11, true),
           (13, 44, status_param, role_param, deny_access, 12, true),
           (13, 38, status_param, role_param, deny_access, 13, true),
           (13, 42, status_param, role_param, deny_access, 14, true),
           (13, 43, status_param, role_param, deny_access, 15, true),
           (13, 37, status_param, role_param, deny_access, 16, true),
           (13, 32, status_param, role_param, deny_access, 17, true),

           (14, 1, status_param, role_param, access, 1, true),
           (14, 3, status_param, role_param, access, 2, false),
           (14, 4, status_param, role_param, access, 3, false),
           (14, 5, status_param, role_param, access, 4, false),
           (14, 34, status_param, role_param, deny_access, 5, true),
           (14, 33, status_param, role_param, deny_access, 6, true),
           (14, 35, status_param, role_param, deny_access, 7, true),
           (14, 36, status_param, role_param, deny_access, 8, true),
           (14, 31, status_param, role_param, deny_access, 9, true),
           (14, 40, status_param, role_param, deny_access, 10, true),
           (14, 39, status_param, role_param, deny_access, 11, true),
           (14, 44, status_param, role_param, deny_access, 12, true),
           (14, 38, status_param, role_param, deny_access, 13, true),
           (14, 42, status_param, role_param, deny_access, 14, true),
           (14, 43, status_param, role_param, deny_access, 15, true),
           (14, 37, status_param, role_param, deny_access, 16, true),
           (14, 32, status_param, role_param, deny_access, 17, true);
END
$$;

CREATE OR REPLACE PROCEDURE insert_epp_template_field_graduate(
    role_param IN epp_template_field.role_id%TYPE,
    status_param IN epp_template_field.status_id%TYPE)
    LANGUAGE plpgsql AS
$$
DECLARE
    access epp_template_field.access%TYPE := 'VIEW';
    deny_access epp_template_field.access%TYPE := 'DENY';
BEGIN
    IF (status_param = 1 OR status_param = 4) THEN
        access := 'EDIT';
    END IF;
    IF (role_param = 3 OR role_param = 4) THEN
        access := 'VIEW';
        deny_access := 'VIEW';
    END IF;
    INSERT INTO epp_template_field (template_id, field_id, status_id, role_id, access, ordinal, is_required)
    VALUES (15, 2, status_param, role_param, access, 1, true),
           (15, 8, status_param, role_param, access, 2, true),
           (15, 1, status_param, role_param, access, 3, true),
           (15, 3, status_param, role_param, access, 4, false),
           (15, 34, status_param, role_param, deny_access, 5, true),
           (15, 35, status_param, role_param, deny_access, 6, true),
           (15, 32, status_param, role_param, deny_access, 7, true),
           (15, 37, status_param, role_param, deny_access, 8, true),
           (15, 33, status_param, role_param, deny_access, 9, true),

           (16, 3, status_param, role_param, access, 1, false),
           (16, 34, status_param, role_param, deny_access, 2, true),
           (16, 35, status_param, role_param, deny_access, 3, true),
           (16, 32, status_param, role_param, deny_access, 4, true),
           (16, 37, status_param, role_param, deny_access, 5, true),
           (16, 33, status_param, role_param, deny_access, 6, true),

           (17, 30, status_param, role_param, access, 1, false),
           (17, 2, status_param, role_param, access, 2, true),
           (17, 6, status_param, role_param, access, 3, false),
           (17, 34, status_param, role_param, deny_access, 4, true),
           (17, 35, status_param, role_param, deny_access, 5, true),
           (17, 32, status_param, role_param, deny_access, 6, true),
           (17, 37, status_param, role_param, deny_access, 7, true),
           (17, 33, status_param, role_param, deny_access, 8, true),
           (18, 34, status_param, role_param, deny_access, 1, true),
           (18, 35, status_param, role_param, deny_access, 2, true),
           (18, 32, status_param, role_param, deny_access, 3, true),
           (18, 37, status_param, role_param, deny_access, 4, true),
           (18, 33, status_param, role_param, deny_access, 5, true),

           (19, 2, status_param, role_param, access, 1, true),
           (19, 6, status_param, role_param, access, 2, false),
           (19, 7, status_param, role_param, access, 3, true),
           (19, 34, status_param, role_param, deny_access, 4, true),
           (19, 35, status_param, role_param, deny_access, 5, true),
           (19, 32, status_param, role_param, deny_access, 6, true),
           (19, 37, status_param, role_param, deny_access, 7, true),
           (19, 33, status_param, role_param, deny_access, 8, true),

           (20, 26, status_param, role_param, access, 1, true),
           (20, 27, status_param, role_param, access, 2, true),
           (20, 34, status_param, role_param, deny_access, 3, true),
           (20, 35, status_param, role_param, deny_access, 4, true),
           (20, 32, status_param, role_param, deny_access, 5, true),
           (20, 37, status_param, role_param, deny_access, 6, true),
           (20, 33, status_param, role_param, deny_access, 7, true),

           (21, 31, status_param, role_param, access, 1, true),
           (21, 40, status_param, role_param, access, 2, true),
           (21, 39, status_param, role_param, access, 3, false),
           (21, 44, status_param, role_param, access, 4, true),
           (21, 38, status_param, role_param, access, 5, true),
           (21, 29, status_param, role_param, access, 6, true),
           (21, 30, status_param, role_param, access, 7, true),
           (21, 6, status_param, role_param, access, 8, true),
           (21, 3, status_param, role_param, access, 9, false),
           (21, 34, status_param, role_param, deny_access, 10, true),
           (21, 35, status_param, role_param, deny_access, 11, true),
           (21, 32, status_param, role_param, deny_access, 12, true),
           (21, 37, status_param, role_param, deny_access, 13, true),
           (21, 33, status_param, role_param, deny_access, 14, true),

           (22, 31, status_param, role_param, access, 1, true),
           (22, 2, status_param, role_param, access, 2, true),
           (22, 6, status_param, role_param, access, 3, true),
           (22, 3, status_param, role_param, access, 4, false),
           (22, 34, status_param, role_param, deny_access, 5, true),
           (22, 35, status_param, role_param, deny_access, 6, true),
           (22, 32, status_param, role_param, deny_access, 7, true),
           (22, 37, status_param, role_param, deny_access, 8, true),
           (22, 33, status_param, role_param, deny_access, 9, true),

           (23, 1, status_param, role_param, access, 1, true),
           (23, 3, status_param, role_param, access, 2, false),
           (23, 34, status_param, role_param, deny_access, 3, true),
           (23, 35, status_param, role_param, deny_access, 4, true),
           (23, 32, status_param, role_param, deny_access, 5, true),
           (23, 37, status_param, role_param, deny_access, 6, true),
           (23, 33, status_param, role_param, deny_access, 7, true),

           (24, 1, status_param, role_param, access, 1, true),
           (24, 3, status_param, role_param, access, 2, false),
           (24, 4, status_param, role_param, access, 3, false),
           (24, 5, status_param, role_param, access, 4, false),
           (24, 34, status_param, role_param, deny_access, 5, true),
           (24, 35, status_param, role_param, deny_access, 6, true),
           (24, 32, status_param, role_param, deny_access, 7, true),
           (24, 37, status_param, role_param, deny_access, 8, true),
           (24, 33, status_param, role_param, deny_access, 9, true),

           (25, 1, status_param, role_param, access, 1, true),
           (25, 3, status_param, role_param, access, 2, false),
           (25, 4, status_param, role_param, access, 3, false),
           (25, 5, status_param, role_param, access, 4, false),
           (25, 34, status_param, role_param, deny_access, 5, true),
           (25, 35, status_param, role_param, deny_access, 6, true),
           (25, 32, status_param, role_param, deny_access, 7, true),
           (25, 37, status_param, role_param, deny_access, 8, true),
           (25, 33, status_param, role_param, deny_access, 9, true);
END
$$;

CREATE OR REPLACE PROCEDURE insert_epp_template_field_manager(
    status_param IN epp_template_field.status_id%TYPE)
    LANGUAGE plpgsql AS
$$
DECLARE
    access epp_template_field.access%TYPE := 'VIEW';
BEGIN
    IF (status_param = 1 OR status_param = 4) THEN
        access := 'EDIT';
    END IF;
    INSERT INTO epp_template_field (template_id, field_id, status_id, role_id, access, ordinal, is_required)
    VALUES (28, 21, status_param, 3, access, 1, true),
           (28, 2, status_param, 3, access, 2, true),
           (28, 3, status_param, 3, access, 3, false),
           (28, 25, status_param, 3, access, 4, true),
           (29, 3, status_param, 3, access, 1, false),
           (29, 6, status_param, 3, access, 2, true),
           (30, 22, status_param, 3, access, 1, true),
           (30, 23, status_param, 3, access, 2, true),
           (30, 24, status_param, 3, access, 3, true),
           (31, 25, status_param, 3, access, 1, true),
           (31, 21, status_param, 3, access, 2, true),
           (31, 2, status_param, 3, access, 3, true),
           (31, 3, status_param, 3, access, 4, false),
           (31, 6, status_param, 3, access, 5, false);
END
$$;

-- Заполняется
CALL insert_epp_template_field_student(1, 1);
-- Опубликована
CALL insert_epp_template_field_student(1, 2);
CALL insert_epp_template_field_student(3, 2);
-- В работе
CALL insert_epp_template_field_student(1, 3);
CALL insert_epp_template_field_student(3, 3);
-- На доработке заявителем
CALL insert_epp_template_field_student(1, 4);
CALL insert_epp_template_field_student(3, 4);
-- Отказ
CALL insert_epp_template_field_student(1, -2);
CALL insert_epp_template_field_student(3, -2);
-- Готова
CALL insert_epp_template_field_student(1, -1);
CALL insert_epp_template_field_student(3, -1);

-- Заполняется
CALL insert_epp_template_field_graduate(1, 1);
-- Опубликована
CALL insert_epp_template_field_graduate(2, 2);
CALL insert_epp_template_field_graduate(3, 2);
-- В работе
CALL insert_epp_template_field_graduate(2, 3);
CALL insert_epp_template_field_graduate(3, 3);
-- На доработке заявителем
CALL insert_epp_template_field_graduate(2, 4);
CALL insert_epp_template_field_graduate(3, 4);
-- Отказ
CALL insert_epp_template_field_graduate(2, -2);
CALL insert_epp_template_field_graduate(3, -2);
-- Готова
CALL insert_epp_template_field_graduate(2, -1);
CALL insert_epp_template_field_graduate(3, -1);

-- Заполняется
CALL insert_epp_template_field_manager(1);
-- Опубликована
CALL insert_epp_template_field_manager(2);
-- В работе
CALL insert_epp_template_field_manager(3);
-- На доработке заявителем
CALL insert_epp_template_field_manager(4);
-- Отказ
CALL insert_epp_template_field_manager(-2);
-- Готова
CALL insert_epp_template_field_manager(-1);

DROP PROCEDURE insert_epp_template_field_student(integer, integer);
DROP PROCEDURE insert_epp_template_field_graduate(integer, integer);
DROP PROCEDURE insert_epp_template_field_manager(integer);