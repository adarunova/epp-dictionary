package ru.hse.edu.eppdictionary.exception.handler;

import io.grpc.Status;
import io.grpc.StatusException;
import lombok.extern.slf4j.Slf4j;
import net.devh.boot.grpc.server.advice.GrpcAdvice;
import net.devh.boot.grpc.server.advice.GrpcExceptionHandler;
import ru.hse.edu.eppdictionary.exception.AlreadyExistsException;
import ru.hse.edu.eppdictionary.exception.NotFoundException;

@GrpcAdvice
@Slf4j
public class GlobalGrpcExceptionHandler {

    @GrpcExceptionHandler(Exception.class)
    public StatusException handleException(Exception e) {
        return buildStatusException(Status.UNKNOWN, e);
    }

    @GrpcExceptionHandler(IllegalArgumentException.class)
    public StatusException handleValidationException(IllegalArgumentException e) {
        return buildStatusException(Status.INVALID_ARGUMENT, e);
    }

    @GrpcExceptionHandler(NotFoundException.class)
    public StatusException handleNotFoundException(NotFoundException e) {
        return buildStatusException(Status.NOT_FOUND, e);
    }

    @GrpcExceptionHandler(AlreadyExistsException.class)
    public StatusException handleAlreadyExistsException(AlreadyExistsException e) {
        return buildStatusException(Status.ALREADY_EXISTS, e);
    }

    private StatusException buildStatusException(Status status, Exception e) {
        logger.error("Error while execution gRPC API, code: {}", status, e);
        return status.withDescription(e.getMessage())
            .withCause(e)
            .asException();
    }

}
