package ru.hse.edu.eppdictionary.mapper.application;

import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import ru.hse.edu.eppdictionary.annotations.BaseGrpcMapping;
import ru.hse.edu.eppdictionary.config.MappersConfig;
import ru.hse.edu.eppdictionary.grpc.FieldRequest;
import ru.hse.edu.eppdictionary.grpc.FieldResponse;
import ru.hse.edu.eppdictionary.mapper.FromProtobufMapper;
import ru.hse.edu.eppdictionary.mapper.ToProtobufMapper;
import ru.hse.edu.eppdictionary.mapper.dictionary.EppFieldMapper;
import ru.hse.edu.eppdictionary.model.application.Application;
import ru.hse.edu.eppdictionary.model.application.ApplicationField;
import ru.hse.edu.eppdictionary.model.dictionary.EppField;

import java.util.List;
import java.util.Map;

@Mapper(
    config = MappersConfig.class,
    uses = {
        EppFieldMapper.class,
        FromProtobufMapper.class,
        ToProtobufMapper.class
    })
public interface ApplicationFieldMapper {

    default List<ApplicationField> toApplicationFields(List<FieldRequest> fields, Application application) {
        return fields.stream()
            .map(field -> toApplicationField(field, application))
            .toList();
    }

    @Mapping(target = "id.applicationId", source = "application.id")
    @Mapping(target = "id.fieldId", source = "field.fieldId")
    @Mapping(target = "application", source = "application")
    @Mapping(target = "field.id", source = "field.fieldId")
    ApplicationField toApplicationField(FieldRequest field, Application application);

    List<FieldResponse> toFieldResponseList(List<ApplicationField> field);

    @BaseGrpcMapping
    @Mapping(target = "id", source = "id.fieldId")
    @Mapping(target = "name.ru", expression = "java(applicationField.getField().getRuName())")
    @Mapping(target = "name.en", expression = "java(applicationField.getField().getEnName())")
    @Mapping(target = "mergeValue", ignore = true)
    @Mapping(target = "mergeName", ignore = true)
    FieldResponse toFieldResponse(ApplicationField applicationField);
}
