package ru.hse.edu.eppdictionary.dto.application;

import lombok.Getter;
import lombok.Setter;

import java.time.Instant;
import java.util.List;

@Getter
@Setter
public class ApplicationFilter {

    private String srId;
    private Integer templateId;
    private Integer statusId;
    private Instant createdDateTimeFrom;
    private Instant createdDateTimeTo;
    private String employeeSrId;

    public boolean hasSrId() {
        return srId != null;
    }

    public boolean hasTemplateId() {
        return templateId != null;
    }

    public boolean hasStatusId() {
        return statusId != null;
    }

    public boolean hasCreatedDateTime() {
        return createdDateTimeFrom != null && createdDateTimeTo != null;
    }

    public boolean hasCreatedDateTimeFrom() {
        return createdDateTimeFrom != null;
    }

    public boolean hasCreatedDateTimeTo() {
        return createdDateTimeTo != null;
    }

    public boolean hasEmployeeSrId() {
        return employeeSrId != null;
    }
}
