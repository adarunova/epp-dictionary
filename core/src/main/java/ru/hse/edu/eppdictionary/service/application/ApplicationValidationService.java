package ru.hse.edu.eppdictionary.service.application;

import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;
import ru.hse.edu.eppdictionary.grpc.FieldRequest;
import ru.hse.edu.eppdictionary.model.application.Application;
import ru.hse.edu.eppdictionary.model.dictionary.EppFieldType;
import ru.hse.edu.eppdictionary.model.dictionary.EppRoleType;
import ru.hse.edu.eppdictionary.model.dictionary.EppStatusType;
import ru.hse.edu.eppdictionary.repository.dictionary.projection.EppFieldWithIsRequired;
import ru.hse.edu.eppdictionary.service.dictionary.EppFieldService;
import ru.hse.edu.eppdictionary.service.dictionary.EppRoleService;
import ru.hse.edu.eppdictionary.service.dictionary.EppStatusRoleValidationService;
import ru.hse.edu.eppdictionary.service.dictionary.EppStatusService;

import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.Set;
import java.util.function.Function;
import java.util.regex.Pattern;
import java.util.stream.Collectors;

@Service
@RequiredArgsConstructor
public class ApplicationValidationService {

    private static final String ENUM_DELIMITER = ";";

    private final EppFieldService eppFieldService;
    private final EppRoleService eppRoleService;
    private final EppStatusService eppStatusService;
    private final EppStatusRoleValidationService eppStatusRoleValidationService;

    /**
     * Валидация переданных полей:
     * 1. На этапе создания заявки - все поля из шаблона переданы.
     * 2. На этапе обновления заявки - переданные поля есть в шаблоне.
     * Для каждого поля проверяются следующие условия:
     * 1. Обязательное поле заполнено
     * 2. Поле типа ENUM содержит корректное значение
     * 3. Поле с regexp'ом корректно в соответствии с паттерном
     */
    public void validateFieldsForTemplate(Integer eppTemplateId, List<FieldRequest> fields, boolean exists) {
        List<EppFieldWithIsRequired> eppFields = eppFieldService.getEppFieldsByEppTemplateId(eppTemplateId);
        validateFieldContainsInParameters(fields, eppFields, exists);

        Map<Integer, EppFieldWithIsRequired> eppFieldById = eppFields.stream()
            .collect(Collectors.toMap(EppFieldWithIsRequired::getId, Function.identity()));

        fields.forEach(field -> validateFieldValue(
            field.hasValue() ? field.getValue().getValue() : null,
            eppFieldById.get(field.getFieldId())
        ));
    }

    /**
     * Валидация статуса и полей при обновлении заявки
     */
    public void validateApplicationForUpdate(Application application, List<FieldRequest> fields,
                                             Integer roleId, Integer statusId) {
        EppStatusType oldStatus = eppStatusService.getStatusById(application.getStatusId());
        EppStatusType newStatus = eppStatusService.getStatusById(statusId);
        EppRoleType role = eppRoleService.getRoleById(roleId);

        if (statusId != null) {
            validateNewStatus(oldStatus, newStatus, role);
        }
        if (!fields.isEmpty()) {
            eppStatusRoleValidationService.validateApplicationInStatusCanBeChangedByPersonWithRole(oldStatus, role);
            validateFieldsForTemplate(application.getTemplateId(), fields, true);
        }
    }

    private void validateFieldContainsInParameters(List<FieldRequest> fields, List<EppFieldWithIsRequired> eppFields,
                                                   boolean exists) {
        Set<Integer> fieldIds = fields.stream()
            .map(FieldRequest::getFieldId)
            .collect(Collectors.toSet());

        Set<Integer> eppFieldIds = eppFields.stream()
            .map(EppFieldWithIsRequired::getId)
            .collect(Collectors.toSet());

        if (!exists && !fieldIds.equals(eppFieldIds) || exists && !eppFieldIds.containsAll(fieldIds)) {
            throw new IllegalArgumentException("Invalid scheme. Expected field ids in '%s'".formatted(eppFieldIds));
        }
    }

    private void validateFieldValue(String value, EppFieldWithIsRequired eppField) {
        if (!StringUtils.hasText(value)) {
            if (eppField.getIsRequired()) {
                throw new IllegalArgumentException("Field id = '%d' must be filled".formatted(eppField.getId()));
            }
            return;
        }

        if (eppField.getType() == EppFieldType.ENUM) {
            validateEnumValue(value, eppField.getEnumList());
        }
        if (eppField.getValidationRegexp() != null) {
            validateValueByRegexp(value, eppField.getValidationRegexp());
        }
    }

    private void validateEnumValue(String value, String enumList) {
        String[] enumValues = enumList.split(ENUM_DELIMITER, -1);
        for (String enumValue : enumValues) {
            if (enumValue.equals(value)) {
                return;
            }
        }
        throw new IllegalArgumentException("Value '%s' is incorrect. Must be one of '%s'".formatted(value, enumList));
    }

    private void validateValueByRegexp(String value, String regexp) {
        if (!Pattern.compile(regexp).matcher(value).matches()) {
            throw new IllegalArgumentException("Value '%s' is incorrect. Must matches pattern '%s'"
                .formatted(value, regexp));
        }
    }

    private void validateNewStatus(EppStatusType oldStatus, EppStatusType newStatus, EppRoleType role) {
        if (Objects.equals(oldStatus, newStatus)) {
            return;
        }
        eppStatusRoleValidationService.validateStatusCanBeChangedToNew(oldStatus, newStatus);
        eppStatusRoleValidationService.validateStatusCanBeChangedByPersonWithRole(oldStatus, role);
    }
}
