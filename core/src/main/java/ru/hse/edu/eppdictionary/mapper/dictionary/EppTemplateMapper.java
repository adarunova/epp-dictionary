package ru.hse.edu.eppdictionary.mapper.dictionary;

import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import ru.hse.edu.eppdictionary.annotations.BaseGrpcMapping;
import ru.hse.edu.eppdictionary.config.MappersConfig;
import ru.hse.edu.eppdictionary.grpc.EppTemplateResponse;
import ru.hse.edu.eppdictionary.grpc.EppTemplatesResponse;
import ru.hse.edu.eppdictionary.mapper.ToProtobufMapper;
import ru.hse.edu.eppdictionary.model.dictionary.EppTemplate;

import java.util.List;

@Mapper(config = MappersConfig.class, uses = ToProtobufMapper.class)
public interface EppTemplateMapper {

    @BaseGrpcMapping
    @Mapping(target = "name.ru", source = "ruName")
    @Mapping(target = "name.en", source = "enName")
    @Mapping(target = "mergeName", ignore = true)
    EppTemplateResponse toEppTemplateResponse(EppTemplate eppTemplate);

    List<EppTemplateResponse> toEppTemplateResponse(List<EppTemplate> eppTemplates);

    default EppTemplatesResponse toEppTemplatesResponse(List<EppTemplate> eppTemplates) {
        return EppTemplatesResponse.newBuilder()
            .addAllEppTemplates(toEppTemplateResponse(eppTemplates))
            .build();
    }
}
