package ru.hse.edu.eppdictionary.config;

import io.envoyproxy.pgv.ReflectiveValidatorIndex;
import io.envoyproxy.pgv.grpc.ValidatingServerInterceptor;
import net.devh.boot.grpc.server.interceptor.GrpcGlobalServerInterceptor;
import org.springframework.context.annotation.Configuration;

@Configuration(proxyBeanMethods = false)
public class GrpcConfig {

    @GrpcGlobalServerInterceptor
    public ValidatingServerInterceptor validatingServerInterceptor() {
        return new ValidatingServerInterceptor(new ReflectiveValidatorIndex());
    }
}
