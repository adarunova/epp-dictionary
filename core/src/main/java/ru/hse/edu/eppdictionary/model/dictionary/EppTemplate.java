package ru.hse.edu.eppdictionary.model.dictionary;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

/**
 * Шаблон заявки
 */
@Getter
@Setter
@Entity
@Table(name = "epp_template")
public class EppTemplate {

    /**
     * Уникальный идентификатор
     */
    @Id
    private int id;

    /**
     * Название заявки на русском языке
     */
    private String ruName;

    /**
     * Название заявки на английском языке
     */
    private String enName;

    /**
     * Идентификатор роли пользователя, который может подавать заявку
     */
    private int roleId;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "roleId", referencedColumnName = "id", insertable = false, updatable = false)
    private EppRole initiator;
}
