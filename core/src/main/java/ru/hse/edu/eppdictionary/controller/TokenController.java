package ru.hse.edu.eppdictionary.controller;

import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.tags.Tag;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import ru.hse.edu.eppdictionary.service.token.TokenService;

@RestController
@RequestMapping(value = "/api/token")
@RequiredArgsConstructor
@Tag(name = "TokenController")
public class TokenController {

    private final TokenService tokenService;

    @PostMapping("/generate-jwt")
    @Operation(
        summary = "Генерация jwt-токена для авторизации в методах gRPC API",
        description = "Метод создан для упрощения тестирования функционала")
    public String generateJwt() {
        return tokenService.generateJwt();
    }
}
