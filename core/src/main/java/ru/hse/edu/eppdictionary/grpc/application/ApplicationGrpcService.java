package ru.hse.edu.eppdictionary.grpc.application;

import com.google.protobuf.Empty;
import io.grpc.stub.StreamObserver;
import lombok.RequiredArgsConstructor;
import net.devh.boot.grpc.server.service.GrpcService;
import ru.hse.edu.eppdictionary.grpc.ApplicationGetByIdRequest;
import ru.hse.edu.eppdictionary.grpc.ApplicationGetByIdResponse;
import ru.hse.edu.eppdictionary.grpc.ApplicationServiceGrpc.ApplicationServiceImplBase;
import ru.hse.edu.eppdictionary.grpc.ApplicationsCreateRequest;
import ru.hse.edu.eppdictionary.grpc.ApplicationsCreateResponse;
import ru.hse.edu.eppdictionary.grpc.ApplicationsGetByFiltersRequest;
import ru.hse.edu.eppdictionary.grpc.ApplicationsGetByFiltersResponse;
import ru.hse.edu.eppdictionary.grpc.ApplicationsGetBySrIdRequest;
import ru.hse.edu.eppdictionary.grpc.ApplicationsGetBySrIdResponse;
import ru.hse.edu.eppdictionary.grpc.ApplicationsUpdateRequest;
import ru.hse.edu.eppdictionary.service.application.ApplicationNotificationService;
import ru.hse.edu.eppdictionary.service.application.ApplicationService;
import ru.hse.edu.eppdictionary.utils.GrpcUtils;

@GrpcService
@RequiredArgsConstructor
public class ApplicationGrpcService extends ApplicationServiceImplBase {

    private final ApplicationService applicationService;
    private final ApplicationNotificationService notificationService;

    @Override
    public void createApplication(ApplicationsCreateRequest request,
                                      StreamObserver<ApplicationsCreateResponse> responseObserver) {
        ApplicationsCreateResponse response = applicationService.createApplication(request);
        notificationService.notifyOnApplicationCreate(response.getId());
        GrpcUtils.sendResponse(responseObserver, response);
    }

    @Override
    public void updateApplicationById(ApplicationsUpdateRequest request, StreamObserver<Empty> responseObserver) {
        applicationService.updateApplication(request);
        notificationService.notifyOnApplicationStatusUpdate(request.getId());
        GrpcUtils.sendResponse(responseObserver, Empty.newBuilder().build());
    }

    @Override
    public void getApplicationsBySrId(ApplicationsGetBySrIdRequest request,
                                      StreamObserver<ApplicationsGetBySrIdResponse> responseObserver) {
        ApplicationsGetBySrIdResponse response = applicationService.getApplicationsBySrId(request);
        GrpcUtils.sendResponse(responseObserver, response);
    }

    @Override
    public void getApplicationById(ApplicationGetByIdRequest request,
                                   StreamObserver<ApplicationGetByIdResponse> responseObserver) {
        ApplicationGetByIdResponse response = applicationService.getApplicationById(request);
        GrpcUtils.sendResponse(responseObserver, response);
    }

    @Override
    public void getApplicationsByFilters(ApplicationsGetByFiltersRequest request,
                                         StreamObserver<ApplicationsGetByFiltersResponse> responseObserver) {
        ApplicationsGetByFiltersResponse response = applicationService.getApplicationsByFilters(request);
        GrpcUtils.sendResponse(responseObserver, response);
    }
}
