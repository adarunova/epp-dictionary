package ru.hse.edu.eppdictionary.mapper.dictionary;

import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.NullValueCheckStrategy;
import ru.hse.edu.eppdictionary.annotations.BaseGrpcMapping;
import ru.hse.edu.eppdictionary.config.MappersConfig;
import ru.hse.edu.eppdictionary.grpc.EppFieldResponse;
import ru.hse.edu.eppdictionary.mapper.ToProtobufMapper;
import ru.hse.edu.eppdictionary.model.dictionary.EppField;

@Mapper(config = MappersConfig.class, uses = ToProtobufMapper.class)
public interface EppFieldMapper {

    @BaseGrpcMapping
    @Mapping(target = "name.ru", source = "ruName")
    @Mapping(target = "name.en", source = "enName")
    @Mapping(target = "enumList", nullValueCheckStrategy = NullValueCheckStrategy.ALWAYS)
    @Mapping(target = "mask", nullValueCheckStrategy = NullValueCheckStrategy.ALWAYS)
    @Mapping(target = "validationRegexp", nullValueCheckStrategy = NullValueCheckStrategy.ALWAYS)
    @Mapping(target = "isMulty", source = "multy", nullValueCheckStrategy = NullValueCheckStrategy.ALWAYS)
    @Mapping(target = "hint", nullValueCheckStrategy = NullValueCheckStrategy.ALWAYS)
    @Mapping(target = "mergeName", ignore = true)
    @Mapping(target = "mergeEnumList", ignore = true)
    @Mapping(target = "mergeMask", ignore = true)
    @Mapping(target = "mergeValidationRegexp", ignore = true)
    @Mapping(target = "mergeHint", ignore = true)
    @Mapping(target = "typeValue", ignore = true)
    EppFieldResponse toEppFieldResponse(EppField eppField);
}
