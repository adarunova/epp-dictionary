package ru.hse.edu.eppdictionary.service.application;

import static ru.hse.edu.eppdictionary.repository.application.ApplicationRepository.hasFilteredValues;

import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import ru.hse.edu.eppdictionary.dto.application.ApplicationFilter;
import ru.hse.edu.eppdictionary.exception.AlreadyExistsException;
import ru.hse.edu.eppdictionary.exception.NotFoundException;
import ru.hse.edu.eppdictionary.grpc.ApplicationGetByIdRequest;
import ru.hse.edu.eppdictionary.grpc.ApplicationGetByIdResponse;
import ru.hse.edu.eppdictionary.grpc.ApplicationsCreateRequest;
import ru.hse.edu.eppdictionary.grpc.ApplicationsCreateResponse;
import ru.hse.edu.eppdictionary.grpc.ApplicationsGetByFiltersRequest;
import ru.hse.edu.eppdictionary.grpc.ApplicationsGetByFiltersResponse;
import ru.hse.edu.eppdictionary.grpc.ApplicationsGetBySrIdRequest;
import ru.hse.edu.eppdictionary.grpc.ApplicationsGetBySrIdResponse;
import ru.hse.edu.eppdictionary.grpc.ApplicationsUpdateRequest;
import ru.hse.edu.eppdictionary.grpc.FieldRequest;
import ru.hse.edu.eppdictionary.mapper.PageableMapper;
import ru.hse.edu.eppdictionary.mapper.application.ApplicationFieldMapper;
import ru.hse.edu.eppdictionary.mapper.application.ApplicationMapper;
import ru.hse.edu.eppdictionary.model.application.Application;
import ru.hse.edu.eppdictionary.model.application.ApplicationField;
import ru.hse.edu.eppdictionary.repository.application.ApplicationRepository;
import ru.hse.edu.eppdictionary.repository.application.projection.ApplicationBaseInfo;

import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.Set;
import java.util.function.Function;
import java.util.stream.Collectors;

@Service
@RequiredArgsConstructor
public class ApplicationService {

    private static final Set<String> SORT_COLUMNS = Set.of("id", "statusId", "createdDateTime", "lastModifiedDateTime");
    private static final String DEFAULT_SORT_COLUMN = "id";
    private static final Set<Integer> END_STATUS_IDS = Set.of(-1, -2);

    private final ApplicationFieldService applicationFieldService;
    private final ApplicationValidationService applicationValidationService;
    private final ApplicationRepository applicationRepository;
    private final ApplicationMapper applicationMapper;
    private final ApplicationFieldMapper applicationFieldMapper;
    private final PageableMapper pageableMapper;

    @Transactional
    public ApplicationsCreateResponse createApplication(ApplicationsCreateRequest request) {
        validateApplicationAlreadyExists(request.getSrId(), request.getTemplateId());
        applicationValidationService.validateFieldsForTemplate(
            request.getTemplateId(), request.getFieldsList(), false
        );

        Application application = applicationMapper.toApplication(request);
        List<ApplicationField> fields =
            applicationFieldMapper.toApplicationFields(request.getFieldsList(), application);

        applicationRepository.save(application);
        applicationFieldService.saveAll(fields);

        return ApplicationsCreateResponse.newBuilder()
            .setId(application.getId())
            .build();
    }

    @Transactional
    public void updateApplication(ApplicationsUpdateRequest request) {
        Application application = applicationRepository
            .findWithFieldsByIdAndStatusIdNotIn(request.getId(), END_STATUS_IDS)
            .orElseThrow(() ->
                new NotFoundException("Active application with id = %d not found".formatted(request.getId())));

        applicationValidationService.validateApplicationForUpdate(
            application, request.getFieldsList(), request.getRoleId(),
            request.hasStatusId() ? request.getStatusId().getValue() : null
        );

        if (!request.getFieldsList().isEmpty()) {
            Map<Integer, FieldRequest> fieldById = request.getFieldsList()
                .stream()
                .collect(Collectors.toMap(FieldRequest::getFieldId, Function.identity()));

            application.getFields().forEach(
                field -> Optional
                    .ofNullable(fieldById.get(field.getId().getFieldId()))
                    .ifPresent(it -> field.setValue(it.getValue().getValue()))
            );
        }

        applicationMapper.fillApplication(application, request);
        applicationRepository.save(application);
    }

    @Transactional(readOnly = true)
    public ApplicationsGetBySrIdResponse getApplicationsBySrId(ApplicationsGetBySrIdRequest request) {
        List<ApplicationBaseInfo> applications = applicationRepository.findAllBySrId(request.getSrId());
        return applicationMapper.toApplicationsGetBySrIdResponse(applications);
    }

    @Transactional(readOnly = true)
    public ApplicationGetByIdResponse getApplicationById(ApplicationGetByIdRequest request) {
        Application application = applicationRepository.findWithAllById(request.getId())
            .orElseThrow(() ->
                new NotFoundException("Application with id = %d not found".formatted(request.getId())));

        return applicationMapper.toApplicationGetByIdResponse(application);
    }

    @Transactional(readOnly = true)
    public ApplicationsGetByFiltersResponse getApplicationsByFilters(ApplicationsGetByFiltersRequest request) {
        ApplicationFilter filter = applicationMapper.toApplicationFilter(request);
        PageRequest pageRequest = pageableMapper.toPageRequest(
            request.hasPageable(), request.getPageable(), DEFAULT_SORT_COLUMN, SORT_COLUMNS
        );

        Page<Application> page = applicationRepository.findAll(hasFilteredValues(filter), pageRequest);

        return applicationMapper.toApplicationsGetByFiltersResponse(
            page.getContent(), pageableMapper.toPageableResponse(page)
        );
    }

    private void validateApplicationAlreadyExists(String srId, Integer templateId) {
        if (applicationRepository.existsBySrIdAndTemplateIdAndStatusIdNotIn(srId, templateId, END_STATUS_IDS)) {
            throw new AlreadyExistsException(
                "Person srId = '%s' already has an active application with templateId = '%d'"
                    .formatted(srId, templateId)
            );
        }
    }

}
