package ru.hse.edu.eppdictionary.model.dictionary;

import lombok.AllArgsConstructor;
import lombok.Getter;

@Getter
@AllArgsConstructor
public enum EppStatusType {

    CREATION("Заполняется", "Filling"),
    PUBLISHED("Создана", "Published"),
    REVIEW("На рассмотрении", "On review"),
    REWORK("Возвращена на доработку", "Rework"),
    REJECTED("Отклонена", "Rejected"),
    DONE("Завершена", "DONE");

    /**
     * Статус на русском языке
     */
    private final String ruName;

    /**
     * Статус на английском языке
     */
    private final String enName;
}
