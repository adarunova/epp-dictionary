package ru.hse.edu.eppdictionary.repository.application;

import org.springframework.data.jpa.domain.Specification;
import org.springframework.data.jpa.repository.EntityGraph;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import ru.hse.edu.eppdictionary.dto.application.ApplicationFilter;
import ru.hse.edu.eppdictionary.model.application.Application;
import ru.hse.edu.eppdictionary.repository.application.projection.ApplicationBaseInfo;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.Set;
import javax.persistence.criteria.JoinType;
import javax.persistence.criteria.Predicate;

@Repository
public interface ApplicationRepository extends JpaRepository<Application, Long>, JpaSpecificationExecutor<Application> {

    boolean existsBySrIdAndTemplateIdAndStatusIdNotIn(String srId, Integer template, Set<Integer> statusIds);

    @EntityGraph(attributePaths = "fields")
    Optional<Application> findWithFieldsByIdAndStatusIdNotIn(Long id, Set<Integer> statusIds);

    @Query("""
        select
        a.id as id,
        et as template,
        es as status,
        a.createdDateTime as createdDateTime,
        a.lastModifiedDateTime as lastModifiedDateTime
        from Application a
        inner join fetch EppTemplate et on et.id = a.templateId
        inner join fetch EppStatus es on es.id = a.statusId
        where a.srId = :srId""")
    List<ApplicationBaseInfo> findAllBySrId(@Param("srId") String srId);

    @EntityGraph(attributePaths = {"template", "status", "fields", "fields.field"})
    Optional<Application> findWithAllById(Long id);

    static Specification<Application> hasFilteredValues(ApplicationFilter filter) {
        return (root, query, cb) -> {
            List<Predicate> predicates = new ArrayList<>();

            if (filter.hasSrId()) {
                predicates.add(cb.equal(root.get("srId"), filter.getSrId()));
            }
            if (filter.hasTemplateId()) {
                predicates.add(cb.equal(root.get("templateId"), filter.getTemplateId()));
            }
            if (filter.hasStatusId()) {
                predicates.add(cb.equal(root.get("statusId"), filter.getStatusId()));
            }
            if (filter.hasCreatedDateTimeFrom()) {
                predicates.add(cb.greaterThanOrEqualTo(root.get("createdDateTime"), filter.getCreatedDateTimeFrom()));
            }
            if (filter.hasCreatedDateTimeTo()) {
                predicates.add(cb.lessThanOrEqualTo(root.get("createdDateTime"), filter.getCreatedDateTimeTo()));
            }

            if (filter.hasEmployeeSrId()) {
                predicates.add(cb.equal(root.get("employeeSrId"), filter.getEmployeeSrId()));
            }

            if (query.getResultType() != Long.class && query.getResultType() != long.class) {
                root.fetch("status", JoinType.LEFT);
                root.fetch("template", JoinType.LEFT);
            }

            return cb.and(predicates.toArray(Predicate[]::new));
        };
    }
}
