package ru.hse.edu.eppdictionary.config.security.grpc;

import io.grpc.reflection.v1alpha.ServerReflectionGrpc;
import net.devh.boot.grpc.server.autoconfigure.GrpcServerSecurityAutoConfiguration;
import net.devh.boot.grpc.server.security.authentication.BearerAuthenticationReader;
import net.devh.boot.grpc.server.security.authentication.GrpcAuthenticationReader;
import net.devh.boot.grpc.server.security.check.AccessPredicate;
import net.devh.boot.grpc.server.security.check.AccessPredicateVoter;
import net.devh.boot.grpc.server.security.check.GrpcSecurityMetadataSource;
import net.devh.boot.grpc.server.security.check.ManualGrpcSecurityMetadataSource;
import org.springframework.boot.autoconfigure.AutoConfigureBefore;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.access.AccessDecisionManager;
import org.springframework.security.access.vote.UnanimousBased;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.ProviderManager;
import ru.hse.edu.eppdictionary.config.security.JwtAuthToken;
import ru.hse.edu.eppdictionary.config.security.JwtAuthenticationProvider;

import java.time.Duration;
import java.time.temporal.ChronoUnit;
import java.util.Collections;

@Configuration
@AutoConfigureBefore(GrpcServerSecurityAutoConfiguration.class)
@EnableConfigurationProperties(GrpcJwtSecurityConfigProperties.class)
public class GrpcJwtSecurityConfig {

    @Bean
    public AuthenticationManager grpcJwtAuthenticationManager(GrpcJwtSecurityConfigProperties properties) {
        return new ProviderManager(Collections.singletonList(
            new JwtAuthenticationProvider(properties.getJwtKey(), Duration.of(60, ChronoUnit.MINUTES))
        ));
    }

    @Bean
    public AccessDecisionManager grpcJwtAccessDecisionManager() {
        return new UnanimousBased(Collections.singletonList(new AccessPredicateVoter()));
    }

    @Bean
    public GrpcAuthenticationReader authenticationReader() {
        return new BearerAuthenticationReader(JwtAuthToken::new);
    }

    @Bean
    public GrpcSecurityMetadataSource grpcSecurityMetadataSource() {
        ManualGrpcSecurityMetadataSource source = new ManualGrpcSecurityMetadataSource();
        source.set(ServerReflectionGrpc.getServiceDescriptor(), AccessPredicate.permitAll());
        source.setDefault(AccessPredicate.fullyAuthenticated());
        return source;
    }

}
