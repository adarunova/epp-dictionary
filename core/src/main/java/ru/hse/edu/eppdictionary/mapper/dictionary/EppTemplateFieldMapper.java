package ru.hse.edu.eppdictionary.mapper.dictionary;

import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import ru.hse.edu.eppdictionary.annotations.BaseGrpcMapping;
import ru.hse.edu.eppdictionary.config.MappersConfig;
import ru.hse.edu.eppdictionary.grpc.EppTemplateFieldResponse;
import ru.hse.edu.eppdictionary.grpc.EppTemplateFieldsResponse;
import ru.hse.edu.eppdictionary.model.dictionary.EppTemplateField;

import java.util.List;

@Mapper(config = MappersConfig.class, uses = EppFieldMapper.class)
public interface EppTemplateFieldMapper {

    List<EppTemplateFieldResponse> toEppTemplateFieldResponseList(List<EppTemplateField> eppTemplateField);

    @BaseGrpcMapping
    @Mapping(target = "isRequired", source = "required")
    @Mapping(target = "mergeField", ignore = true)
    @Mapping(target = "accessValue", ignore = true)
    EppTemplateFieldResponse toEppTemplateFieldResponse(EppTemplateField eppTemplateField);

    default EppTemplateFieldsResponse toEppTemplateFieldsResponse(List<EppTemplateField> eppTemplateField) {
        return EppTemplateFieldsResponse.newBuilder()
            .addAllFields(toEppTemplateFieldResponseList(eppTemplateField))
            .build();
    }
}
