package ru.hse.edu.eppdictionary.model.dictionary;

public enum EppFieldType {

    INPUT,
    TEXTAREA,
    FILE,
    ENUM,
    REFERENCE,
    BOOLEAN,
    HIDDEN,
    DATE
}
