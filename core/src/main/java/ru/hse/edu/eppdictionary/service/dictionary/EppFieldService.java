package ru.hse.edu.eppdictionary.service.dictionary;

import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import ru.hse.edu.eppdictionary.repository.dictionary.EppFieldRepository;
import ru.hse.edu.eppdictionary.repository.dictionary.projection.EppFieldWithIsRequired;

import java.util.List;

@Service
@RequiredArgsConstructor
public class EppFieldService {

    private final EppFieldRepository eppFieldRepository;

    @Transactional(readOnly = true)
    public List<EppFieldWithIsRequired> getEppFieldsByEppTemplateId(Integer eppTemplateId) {
        return eppFieldRepository.findAllByEppTemplateId(eppTemplateId);
    }
}
