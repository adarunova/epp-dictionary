package ru.hse.edu.eppdictionary.model.dictionary;

import lombok.AllArgsConstructor;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.io.Serial;
import java.io.Serializable;
import javax.persistence.Embeddable;

@Getter
@Setter
@Embeddable
@EqualsAndHashCode
@NoArgsConstructor
@AllArgsConstructor
public class EppTemplateFieldId implements Serializable {

    @Serial
    private static final long serialVersionUID = 1170631182278065647L;

    /**
     * Идентификатор поля
     */
    private Integer fieldId;

    /**
     * Идентификатор шаблона заявления
     */
    private Integer templateId;

    /**
     * Идентификатор роли пользователя
     */
    private Integer roleId;

    /**
     * Идентификатор текущего статуса заявки
     */
    private Integer statusId;
}
