package ru.hse.edu.eppdictionary.grpc.dictionary;

import io.grpc.stub.StreamObserver;
import lombok.RequiredArgsConstructor;
import net.devh.boot.grpc.server.service.GrpcService;
import ru.hse.edu.eppdictionary.grpc.EppTemplateServiceGrpc.EppTemplateServiceImplBase;
import ru.hse.edu.eppdictionary.grpc.EppTemplatesByRoleRequest;
import ru.hse.edu.eppdictionary.grpc.EppTemplatesResponse;
import ru.hse.edu.eppdictionary.service.dictionary.EppTemplateService;
import ru.hse.edu.eppdictionary.utils.GrpcUtils;

@GrpcService
@RequiredArgsConstructor
public class EppTemplateGrpcService extends EppTemplateServiceImplBase {

    private final EppTemplateService eppTemplateService;

    @Override
    public void getEppTemplatesByRole(EppTemplatesByRoleRequest request,
                                     StreamObserver<EppTemplatesResponse> responseObserver) {
        EppTemplatesResponse response = eppTemplateService.getEppTemplatesByRole(request);
        GrpcUtils.sendResponse(responseObserver, response);
    }
}
