package ru.hse.edu.eppdictionary.service.dictionary;

import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import ru.hse.edu.eppdictionary.grpc.EppTemplatesByRoleRequest;
import ru.hse.edu.eppdictionary.grpc.EppTemplatesResponse;
import ru.hse.edu.eppdictionary.mapper.dictionary.EppTemplateMapper;
import ru.hse.edu.eppdictionary.model.dictionary.EppTemplate;
import ru.hse.edu.eppdictionary.repository.dictionary.EppTemplateRepository;

import java.util.List;

@Service
@RequiredArgsConstructor
public class EppTemplateService {

    private final EppTemplateRepository eppTemplateRepository;
    private final EppTemplateMapper eppTemplateMapper;

    @Transactional(readOnly = true)
    public EppTemplatesResponse getEppTemplatesByRole(EppTemplatesByRoleRequest request) {
        List<EppTemplate> eppTemplates = eppTemplateRepository.findAllByRoleId(request.getRoleId());
        return eppTemplateMapper.toEppTemplatesResponse(eppTemplates);
    }
}
