package ru.hse.edu.eppdictionary.repository.dictionary;

import org.springframework.data.jpa.repository.EntityGraph;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import ru.hse.edu.eppdictionary.model.dictionary.EppTemplateField;
import ru.hse.edu.eppdictionary.model.dictionary.EppTemplateFieldId;

import java.util.List;

@Repository
public interface EppTemplateFieldRepository extends JpaRepository<EppTemplateField, EppTemplateFieldId> {

    @EntityGraph(attributePaths = "field")
    List<EppTemplateField> findByIdTemplateIdAndIdRoleIdAndIdStatusId(Integer templateId, Integer roleId,
                                                                      Integer statusId);
}
