package ru.hse.edu.eppdictionary.model.application;

import lombok.AllArgsConstructor;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.io.Serial;
import java.io.Serializable;
import javax.persistence.Embeddable;

@Getter
@Setter
@Embeddable
@EqualsAndHashCode
@NoArgsConstructor
@AllArgsConstructor
public class ApplicationFieldId implements Serializable {

    @Serial
    private static final long serialVersionUID = -7563602345078010256L;

    /**
     * Идентификатор заявки
     */
    private Long applicationId;

    /**
     * Идентификатор поля
     */
    private Integer fieldId;
}
