package ru.hse.edu.eppdictionary.mapper;

import com.google.protobuf.Int32Value;
import com.google.protobuf.StringValue;
import com.google.protobuf.Timestamp;
import org.mapstruct.Mapper;
import ru.hse.edu.eppdictionary.config.MappersConfig;

import java.time.Instant;

@Mapper(config = MappersConfig.class)
public interface FromProtobufMapper {

    default String toString(StringValue value) {
        return value.getValue();
    }

    default Integer toInteger(Int32Value value) {
        return value.getValue();
    }

    default Instant toInstant(Timestamp value) {
        return Instant.ofEpochSecond(value.getSeconds(), value.getNanos());
    }
}

