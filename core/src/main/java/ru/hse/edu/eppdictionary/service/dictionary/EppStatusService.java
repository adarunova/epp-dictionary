package ru.hse.edu.eppdictionary.service.dictionary;

import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import ru.hse.edu.eppdictionary.exception.NotFoundException;
import ru.hse.edu.eppdictionary.model.dictionary.EppStatusType;
import ru.hse.edu.eppdictionary.repository.dictionary.EppStatusRepository;

@Service
@RequiredArgsConstructor
public class EppStatusService {

    private final EppStatusRepository eppStatusRepository;

    @Transactional(readOnly = true)
    public EppStatusType getStatusById(Integer id) {
        return eppStatusRepository.findById(id)
            .orElseThrow(() -> new NotFoundException("Status with id = '%d' not found".formatted(id)))
            .getStatus();
    }
}
