package ru.hse.edu.eppdictionary.model.application;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;
import ru.hse.edu.eppdictionary.model.dictionary.EppField;

import javax.persistence.CascadeType;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.MapsId;
import javax.persistence.Table;

/**
 * Связь поданной заявки с заполненным её полем
 */
@Getter
@Setter
@Entity
@Table(name = "application_field")
public class ApplicationField {

    @EmbeddedId
    private ApplicationFieldId id;

    @MapsId("applicationId")
    @ManyToOne(fetch = FetchType.LAZY)
    private Application application;

    @MapsId("fieldId")
    @ManyToOne(fetch = FetchType.LAZY)
    private EppField field;

    /**
     * Значение заполненного поля в заявке
     */
    private String value;

}
