package ru.hse.edu.eppdictionary.mapper.application;

import org.mapstruct.CollectionMappingStrategy;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.MappingTarget;
import org.mapstruct.NullValueCheckStrategy;
import ru.hse.edu.eppdictionary.annotations.BaseGrpcMapping;
import ru.hse.edu.eppdictionary.config.MappersConfig;
import ru.hse.edu.eppdictionary.dto.application.ApplicationFilter;
import ru.hse.edu.eppdictionary.grpc.ApplicationGetByIdResponse;
import ru.hse.edu.eppdictionary.grpc.ApplicationGetResponse;
import ru.hse.edu.eppdictionary.grpc.ApplicationsCreateRequest;
import ru.hse.edu.eppdictionary.grpc.ApplicationsGetByFiltersRequest;
import ru.hse.edu.eppdictionary.grpc.ApplicationsGetByFiltersResponse;
import ru.hse.edu.eppdictionary.grpc.ApplicationsGetBySrIdResponse;
import ru.hse.edu.eppdictionary.grpc.ApplicationsUpdateRequest;
import ru.hse.edu.eppdictionary.grpc.PageableResponse;
import ru.hse.edu.eppdictionary.mapper.FromProtobufMapper;
import ru.hse.edu.eppdictionary.mapper.ToProtobufMapper;
import ru.hse.edu.eppdictionary.mapper.dictionary.EppStatusMapper;
import ru.hse.edu.eppdictionary.mapper.dictionary.EppTemplateMapper;
import ru.hse.edu.eppdictionary.model.application.Application;
import ru.hse.edu.eppdictionary.repository.application.projection.ApplicationBaseInfo;

import java.util.List;

@Mapper(
    config = MappersConfig.class,
    uses = {
        ApplicationFieldMapper.class,
        EppTemplateMapper.class,
        EppStatusMapper.class,
        ToProtobufMapper.class,
        FromProtobufMapper.class
    },
    collectionMappingStrategy = CollectionMappingStrategy.SETTER_PREFERRED
)
public interface ApplicationMapper {

    @Mapping(target = "id", ignore = true)
    @Mapping(target = "response", ignore = true)
    @Mapping(target = "createdDateTime", ignore = true)
    @Mapping(target = "lastModifiedDateTime", ignore = true)
    @Mapping(target = "employeeSrId", ignore = true)
    @Mapping(target = "template", ignore = true)
    @Mapping(target = "statusId", expression = "java(2)")
    @Mapping(target = "status", ignore = true)
    @Mapping(target = "fields", ignore = true)
    Application toApplication(ApplicationsCreateRequest request);

    @Mapping(target = "id", ignore = true)
    @Mapping(target = "srId", ignore = true)
    @Mapping(target = "templateId", ignore = true)
    @Mapping(target = "createdDateTime", ignore = true)
    @Mapping(target = "lastModifiedDateTime", ignore = true)
    @Mapping(target = "template", ignore = true)
    @Mapping(target = "status", ignore = true)
    @Mapping(target = "fields", ignore = true)
    void fillApplication(@MappingTarget Application application, ApplicationsUpdateRequest request);

    @BaseGrpcMapping
    @Mapping(target = "mergeTemplate", ignore = true)
    @Mapping(target = "mergeStatus", ignore = true)
    @Mapping(target = "mergeCreatedDateTime", ignore = true)
    @Mapping(target = "mergeLastModifiedDateTime", ignore = true)
    ApplicationGetResponse toApplicationGetResponse(ApplicationBaseInfo application);

    @BaseGrpcMapping
    @Mapping(target = "mergeTemplate", ignore = true)
    @Mapping(target = "mergeStatus", ignore = true)
    @Mapping(target = "mergeCreatedDateTime", ignore = true)
    @Mapping(target = "mergeLastModifiedDateTime", ignore = true)
    ApplicationGetResponse toApplicationGetResponse(Application application);

    List<ApplicationGetResponse> toApplicationGetResponseList(List<ApplicationBaseInfo> applications);

    default ApplicationsGetBySrIdResponse toApplicationsGetBySrIdResponse(List<ApplicationBaseInfo> applications) {
        return ApplicationsGetBySrIdResponse.newBuilder()
            .addAllApplications(toApplicationGetResponseList(applications))
            .build();
    }

    @BaseGrpcMapping
    @Mapping(target = "fieldsList", source = "fields")
    @Mapping(target = "response", nullValueCheckStrategy = NullValueCheckStrategy.ALWAYS)
    @Mapping(target = "employeeSrId", nullValueCheckStrategy = NullValueCheckStrategy.ALWAYS)
    @Mapping(target = "mergeTemplate", ignore = true)
    @Mapping(target = "mergeStatus", ignore = true)
    @Mapping(target = "mergeCreatedDateTime", ignore = true)
    @Mapping(target = "mergeLastModifiedDateTime", ignore = true)
    @Mapping(target = "mergeEmployeeSrId", ignore = true)
    @Mapping(target = "mergeResponse", ignore = true)
    @Mapping(target = "removeFields", ignore = true)
    @Mapping(target = "fieldsOrBuilderList", ignore = true)
    @Mapping(target = "fieldsBuilderList", ignore = true)
    ApplicationGetByIdResponse toApplicationGetByIdResponse(Application application);

    ApplicationFilter toApplicationFilter(ApplicationsGetByFiltersRequest request);

    default ApplicationsGetByFiltersResponse toApplicationsGetByFiltersResponse(List<Application> applications,
                                                                                PageableResponse pageable) {
        return ApplicationsGetByFiltersResponse.newBuilder()
            .addAllApplications(
                applications.stream()
                    .map(this::toApplicationGetResponse)
                    .toList()
            )
            .setPageable(pageable)
            .build();
    }
}
