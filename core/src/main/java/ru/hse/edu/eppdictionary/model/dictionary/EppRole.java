package ru.hse.edu.eppdictionary.model.dictionary;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 * Роли пользователей сервисом
 */
@Getter
@Setter
@Entity
@Table(name = "epp_role")
public class EppRole {

    /**
     * Уникальный идентификатор
     */
    @Id
    private int id;

    /**
     * Роль
     */
    @Enumerated(EnumType.STRING)
    private EppRoleType role;
}
