package ru.hse.edu.eppdictionary.model.mail;

import lombok.AllArgsConstructor;
import lombok.Getter;

@AllArgsConstructor
@Getter
public enum NotificationMail {

    CREATE(
        "Создана новая заявка",
        """
            Здравствуйте!
            Создана новая заявка с номером №%s.
            """
    ),
    UPDATE(
        "Ваша заявка обновлена",
        """
            Здравствуйте!
            Статус заявки №%s обновлён.
            """
    );

    private final String subject;
    private final String message;

}
