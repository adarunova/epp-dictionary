package ru.hse.edu.eppdictionary.repository.dictionary;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import ru.hse.edu.eppdictionary.model.dictionary.EppTemplate;

import java.util.List;

@Repository
public interface EppTemplateRepository extends JpaRepository<EppTemplate, Integer> {

    List<EppTemplate> findAllByRoleId(Integer roleId);
}
