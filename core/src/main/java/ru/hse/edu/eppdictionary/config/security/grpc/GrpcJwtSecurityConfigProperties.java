package ru.hse.edu.eppdictionary.config.security.grpc;

import lombok.Getter;
import lombok.Setter;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.validation.annotation.Validated;

import javax.validation.constraints.NotNull;

@Getter
@Setter
@Validated
@ConfigurationProperties(prefix = "grpc-security")
public class GrpcJwtSecurityConfigProperties {

    @NotNull
    private String jwtKey;
}
