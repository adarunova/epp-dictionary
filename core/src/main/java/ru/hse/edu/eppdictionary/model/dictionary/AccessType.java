package ru.hse.edu.eppdictionary.model.dictionary;

public enum AccessType {

    DENY,
    VIEW,
    EDIT
}
