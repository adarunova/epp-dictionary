package ru.hse.edu.eppdictionary.repository.dictionary.projection;

import lombok.Getter;
import lombok.Setter;
import lombok.experimental.Accessors;
import ru.hse.edu.eppdictionary.model.dictionary.EppFieldType;

@Getter
@Setter
public class EppFieldWithIsRequired {

    private Integer id;
    private EppFieldType type;
    private String enumList;
    private String validationRegexp;
    private Boolean isRequired;
}
