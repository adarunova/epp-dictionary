package ru.hse.edu.eppdictionary.model.application;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;
import lombok.experimental.Accessors;
import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.annotation.LastModifiedDate;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;
import ru.hse.edu.eppdictionary.model.dictionary.EppStatus;
import ru.hse.edu.eppdictionary.model.dictionary.EppTemplate;

import java.time.Instant;
import java.util.ArrayList;
import java.util.List;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EntityListeners;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;

/**
 * Заявка
 */
@Getter
@Setter
@Entity
@Table(name = "application")
@EntityListeners(AuditingEntityListener.class)
@Accessors(chain = true)
public class Application {

    /**
     * Идентификатор заявки
     */
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    /**
     * Внешний идентификатор заявителя (из SmartReg)
     */
    private String srId;

    /**
     * Идентификатор шаблона заявления
     */
    private Integer templateId;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "templateId", referencedColumnName = "id", insertable = false, updatable = false)
    private EppTemplate template;

    /**
     * Идентификатор текущего статуса заявки
     */
    private Integer statusId;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "statusId", referencedColumnName = "id", insertable = false, updatable = false)
    private EppStatus status;

    /**
     * Информация об ответе на заявку
     */
    private String response;

    /**
     * Дата создания заявления
     */
    @CreatedDate
    @Column(name = "created_dttm")
    private Instant createdDateTime;

    /**
     * Дата изменения записи
     */
    @LastModifiedDate
    @Column(name = "last_modified_dttm")
    private Instant lastModifiedDateTime;

    private String employeeSrId;

    @OneToMany(mappedBy = "application")
    private List<ApplicationField> fields = new ArrayList<>();
}
