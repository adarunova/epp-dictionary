package ru.hse.edu.eppdictionary.repository.application;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import ru.hse.edu.eppdictionary.model.application.ApplicationField;
import ru.hse.edu.eppdictionary.model.application.ApplicationFieldId;

import java.util.Optional;

@Repository
public interface ApplicationFieldRepository extends JpaRepository<ApplicationField, ApplicationFieldId> {

    @Query("""
            select af.value from ApplicationField af
            where af.application.id=:applicationId and af.field.id=37
            """)
    Optional<String> findEmailByApplicationId(@Param("applicationId") Long applicationId);
}
