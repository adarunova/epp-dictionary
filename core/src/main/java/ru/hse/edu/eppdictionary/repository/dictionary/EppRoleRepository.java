package ru.hse.edu.eppdictionary.repository.dictionary;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import ru.hse.edu.eppdictionary.model.dictionary.EppRole;

@Repository
public interface EppRoleRepository extends JpaRepository<EppRole, Integer> {
}
