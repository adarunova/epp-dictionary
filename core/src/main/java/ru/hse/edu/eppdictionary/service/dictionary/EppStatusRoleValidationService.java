package ru.hse.edu.eppdictionary.service.dictionary;

import static ru.hse.edu.eppdictionary.model.dictionary.EppStatusType.CREATION;
import static ru.hse.edu.eppdictionary.model.dictionary.EppStatusType.DONE;
import static ru.hse.edu.eppdictionary.model.dictionary.EppStatusType.PUBLISHED;
import static ru.hse.edu.eppdictionary.model.dictionary.EppStatusType.REJECTED;
import static ru.hse.edu.eppdictionary.model.dictionary.EppStatusType.REVIEW;
import static ru.hse.edu.eppdictionary.model.dictionary.EppStatusType.REWORK;

import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import ru.hse.edu.eppdictionary.model.dictionary.EppRoleType;
import ru.hse.edu.eppdictionary.model.dictionary.EppStatusType;

import java.util.EnumSet;
import java.util.Set;

@Service
@RequiredArgsConstructor
public class EppStatusRoleValidationService {

    public void validateStatusCanBeChangedToNew(EppStatusType oldStatus, EppStatusType newStatus) {
        boolean isStatusOneOf = switch (oldStatus) {
            case CREATION -> isStatusOneOf(EnumSet.of(PUBLISHED), newStatus);
            case PUBLISHED, REWORK -> isStatusOneOf(EnumSet.of(REVIEW), newStatus);
            case REVIEW -> isStatusOneOf(EnumSet.of(REWORK, REJECTED, DONE), newStatus);
            case REJECTED, DONE -> false;
        };

        if (!isStatusOneOf) {
            throw new IllegalArgumentException("Failed to change status to '%s'".formatted(newStatus));
        }
    }

    public void validateStatusCanBeChangedByPersonWithRole(EppStatusType oldStatus, EppRoleType role) {
        boolean isStatusOneOf = switch (role) {
            // CREATION -> PUBLISHED; REWORK -> REVIEW
            case STUDENT, GRADUATE -> isStatusOneOf(EnumSet.of(CREATION, REWORK), oldStatus);
            // PUBLISHED -> REVIEW; REVIEW -> REWORK, REJECTED, DONE
            case MANAGER_PROGRAM, EMPLOYEE -> isStatusOneOf(EnumSet.of(PUBLISHED, REVIEW), oldStatus);
            default -> throw new RuntimeException("Unknown role '%s'".formatted(role));
        };

        if (!isStatusOneOf) {
            throw new IllegalArgumentException(
                "Status can't be changed from '%s' by person with role '%s'".formatted(oldStatus, role)
            );
        }
    }

    public void validateApplicationInStatusCanBeChangedByPersonWithRole(EppStatusType status, EppRoleType role) {
        boolean isStatusOneOf = switch (role) {
            case STUDENT, GRADUATE -> isStatusOneOf(EnumSet.of(REWORK), status);
            case MANAGER_PROGRAM, EMPLOYEE -> isStatusOneOf(EnumSet.of(PUBLISHED, REVIEW), status);
            default -> throw new RuntimeException("Unknown role '%s'".formatted(role));
        };

        if (!isStatusOneOf) {
            throw new IllegalArgumentException(
                "Application in status '%s' can't be changed by person with role '%s'".formatted(status, role)
            );
        }
    }

    private boolean isStatusOneOf(Set<EppStatusType> expected, EppStatusType actual) {
        return expected.contains(actual);
    }
}
