package ru.hse.edu.eppdictionary.model.dictionary;

import lombok.Getter;
import lombok.Setter;
import ru.hse.edu.eppdictionary.model.application.Application;
import ru.hse.edu.eppdictionary.model.application.ApplicationField;

import java.util.List;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToMany;
import javax.persistence.Table;

/**
 * Поле в шаблоне заявления
 */
@Getter
@Setter
@Entity
@Table(name = "epp_field")
public class EppField {

    /**
     * Уникальный идентификатор
     */
    @Id
    private Integer id;

    /**
     * Название поля заявки на русском языке
     */
    private String ruName;

    /**
     * Название поля заявки на английском языке
     */
    private String enName;

    /**
     * Тип поля для отображения на фронте
     */
    @Enumerated(EnumType.STRING)
    private EppFieldType type;

    /**
     * Перечисление значений поля (если поле enum)
     */
    private String enumList;

    /**
     * Маска, по которой заполняется поле
     */
    private String mask;

    /**
     * Regexp для валидации значения поля
     */
    private String validationRegexp;

    /**
     * Флаг мультиселекта
     */
    private boolean isMulty;

    /**
     * Подсказка
     */
    private String hint;
}
