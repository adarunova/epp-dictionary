package ru.hse.edu.eppdictionary.service.application;

import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import ru.hse.edu.eppdictionary.exception.NotFoundException;
import ru.hse.edu.eppdictionary.model.application.ApplicationField;
import ru.hse.edu.eppdictionary.repository.application.ApplicationFieldRepository;

import java.util.List;

@Service
@RequiredArgsConstructor
public class ApplicationFieldService {

    private final ApplicationFieldRepository applicationFieldRepository;

    @Transactional
    public void saveAll(List<ApplicationField> fields) {
        applicationFieldRepository.saveAll(fields);
    }

    @Transactional
    public String getEmailByApplicationId(Long applicationId) {
        return applicationFieldRepository.findEmailByApplicationId(applicationId)
            .orElseThrow(() ->
                new NotFoundException("There is no email to send message to"));
    }
}
