package ru.hse.edu.eppdictionary.mapper;

import com.google.protobuf.StringValue;
import com.google.protobuf.Timestamp;
import org.mapstruct.Mapper;
import ru.hse.edu.eppdictionary.config.MappersConfig;

import java.time.Instant;

@Mapper(config = MappersConfig.class)
public interface ToProtobufMapper {

    default StringValue toStringValue(String value) {
        return value == null ? null : StringValue.of(value);
    }

    default Timestamp toTimestamp(Instant instant) {
        return instant == null
            ? null
            : Timestamp.newBuilder().setSeconds(instant.getEpochSecond()).setNanos(instant.getNano()).build();
    }

}

