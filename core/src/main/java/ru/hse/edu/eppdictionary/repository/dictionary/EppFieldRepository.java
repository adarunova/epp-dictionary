package ru.hse.edu.eppdictionary.repository.dictionary;

import lombok.RequiredArgsConstructor;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;
import ru.hse.edu.eppdictionary.repository.dictionary.projection.EppFieldWithIsRequired;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Repository
@RequiredArgsConstructor
public class EppFieldRepository {

    private static final String FIND_ALL_BY_TEMPLATE_ID_QUERY = """
        select distinct ef.id, ef.type, ef.enum_list, ef.validation_regexp, etf.is_required
        from epp_field ef
        inner join epp_template_field etf on ef.id = etf.field_id
        where etf.template_id = :templateId""";

    private final NamedParameterJdbcTemplate namedParameterJdbcTemplate;

    @Transactional(readOnly = true)
    public List<EppFieldWithIsRequired> findAllByEppTemplateId(Integer templateId) {
        Map<String, Integer> params = new HashMap<>();
        params.put("templateId", templateId);

        return namedParameterJdbcTemplate.query(
            FIND_ALL_BY_TEMPLATE_ID_QUERY, params, new BeanPropertyRowMapper<>(EppFieldWithIsRequired.class)
        );
    }
}
