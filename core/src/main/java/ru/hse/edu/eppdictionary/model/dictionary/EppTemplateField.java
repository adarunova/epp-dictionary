package ru.hse.edu.eppdictionary.model.dictionary;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

/**
 * Связь заявки с полем и их отображение в зависимости от роли и статуса
 */
@Getter
@Setter
@Entity
@Table(name = "epp_template_field")
public class EppTemplateField {

    @EmbeddedId
    private EppTemplateFieldId id;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "fieldId", referencedColumnName = "id", insertable = false, updatable = false)
    private EppField field;

    /**
     * Доступность поля в заявке в определённом статусе для пользователя с определённой ролью
     */
    @Enumerated(EnumType.STRING)
    private AccessType access;

    /**
     * Порядок отображения поля в заявке
     */
    private int ordinal;

    /**
     * Обязательность поля
     */
    private boolean isRequired;
}
