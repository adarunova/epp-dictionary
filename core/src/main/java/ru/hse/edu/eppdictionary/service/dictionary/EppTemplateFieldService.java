package ru.hse.edu.eppdictionary.service.dictionary;

import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import ru.hse.edu.eppdictionary.grpc.EppTemplateFieldsRequest;
import ru.hse.edu.eppdictionary.grpc.EppTemplateFieldsResponse;
import ru.hse.edu.eppdictionary.mapper.dictionary.EppTemplateFieldMapper;
import ru.hse.edu.eppdictionary.model.dictionary.EppField;
import ru.hse.edu.eppdictionary.model.dictionary.EppTemplateField;
import ru.hse.edu.eppdictionary.repository.dictionary.EppTemplateFieldRepository;

import java.util.List;

@Service
@RequiredArgsConstructor
public class EppTemplateFieldService {

    private final EppTemplateFieldRepository eppTemplateFieldRepository;
    private final EppTemplateFieldMapper eppTemplateFieldMapper;

    @Transactional(readOnly = true)
    public EppTemplateFieldsResponse getEppTemplateField(EppTemplateFieldsRequest request) {
        List<EppTemplateField> eppTemplateField = eppTemplateFieldRepository
            .findByIdTemplateIdAndIdRoleIdAndIdStatusId(
                request.getTemplateId(),
                request.getRoleId(),
                request.getStatusId()
            );

        return eppTemplateFieldMapper.toEppTemplateFieldsResponse(eppTemplateField);
    }
}
