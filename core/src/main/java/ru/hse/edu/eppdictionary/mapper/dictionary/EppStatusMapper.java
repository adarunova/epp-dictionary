package ru.hse.edu.eppdictionary.mapper.dictionary;

import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import ru.hse.edu.eppdictionary.annotations.BaseGrpcMapping;
import ru.hse.edu.eppdictionary.config.MappersConfig;
import ru.hse.edu.eppdictionary.grpc.EppStatusResponse;
import ru.hse.edu.eppdictionary.model.dictionary.EppStatus;

@Mapper(config = MappersConfig.class)
public interface EppStatusMapper {

    @BaseGrpcMapping
    @Mapping(target = "name.ru", expression = "java(eppStatus.getStatus().getRuName())")
    @Mapping(target = "name.en", expression = "java(eppStatus.getStatus().getEnName())")
    @Mapping(target = "mergeName", ignore = true)
    EppStatusResponse toEppStatusResponse(EppStatus eppStatus);
}
