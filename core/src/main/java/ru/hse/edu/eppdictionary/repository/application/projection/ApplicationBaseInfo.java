package ru.hse.edu.eppdictionary.repository.application.projection;

import ru.hse.edu.eppdictionary.model.dictionary.EppStatus;
import ru.hse.edu.eppdictionary.model.dictionary.EppTemplate;

import java.time.Instant;

public interface ApplicationBaseInfo {

    Long getId();

    EppTemplate getTemplate();

    EppStatus getStatus();

    Instant getCreatedDateTime();

    Instant getLastModifiedDateTime();
}
