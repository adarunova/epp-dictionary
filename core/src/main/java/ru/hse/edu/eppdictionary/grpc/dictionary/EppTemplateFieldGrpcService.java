package ru.hse.edu.eppdictionary.grpc.dictionary;

import io.grpc.stub.StreamObserver;
import lombok.RequiredArgsConstructor;
import net.devh.boot.grpc.server.service.GrpcService;
import ru.hse.edu.eppdictionary.grpc.EppTemplateFieldServiceGrpc.EppTemplateFieldServiceImplBase;
import ru.hse.edu.eppdictionary.grpc.EppTemplateFieldsRequest;
import ru.hse.edu.eppdictionary.grpc.EppTemplateFieldsResponse;
import ru.hse.edu.eppdictionary.service.dictionary.EppTemplateFieldService;
import ru.hse.edu.eppdictionary.utils.GrpcUtils;

@GrpcService
@RequiredArgsConstructor
public class EppTemplateFieldGrpcService extends EppTemplateFieldServiceImplBase {

    private final EppTemplateFieldService eppTemplateFieldService;

    @Override
    public void getEppTemplateFields(EppTemplateFieldsRequest request,
                                     StreamObserver<EppTemplateFieldsResponse> responseObserver) {
        EppTemplateFieldsResponse response = eppTemplateFieldService.getEppTemplateField(request);
        GrpcUtils.sendResponse(responseObserver, response);
    }

}
