package ru.hse.edu.eppdictionary.service.dictionary;

import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import ru.hse.edu.eppdictionary.exception.NotFoundException;
import ru.hse.edu.eppdictionary.model.dictionary.EppRoleType;
import ru.hse.edu.eppdictionary.repository.dictionary.EppRoleRepository;

@Service
@RequiredArgsConstructor
public class EppRoleService {

    private final EppRoleRepository eppRoleRepository;

    @Transactional(readOnly = true)
    public EppRoleType getRoleById(Integer id) {
        return eppRoleRepository.findById(id)
            .orElseThrow(() -> new NotFoundException("Role with id = '%d' not found".formatted(id)))
            .getRole();
    }
}
