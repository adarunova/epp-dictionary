package ru.hse.edu.eppdictionary;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cache.annotation.EnableCaching;
import org.springframework.data.jpa.repository.config.EnableJpaAuditing;

@SpringBootApplication
@EnableCaching
@EnableJpaAuditing
public class EppDictionaryApplication {

    public static void main(String[] args) {
        SpringApplication.run(EppDictionaryApplication.class, args);
    }
}