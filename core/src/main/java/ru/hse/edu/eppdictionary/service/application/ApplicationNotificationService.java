package ru.hse.edu.eppdictionary.service.application;

import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;
import ru.hse.edu.eppdictionary.model.mail.Mail;
import ru.hse.edu.eppdictionary.model.mail.NotificationMail;
import ru.hse.edu.eppdictionary.service.mail.MailService;

@Service
@AllArgsConstructor
public class ApplicationNotificationService {

    private final ApplicationFieldService applicationFieldService;
    private final MailService mailService;

    public void notifyOnApplicationCreate(Long applicationId) {
        String recipientEmail = applicationFieldService.getEmailByApplicationId(applicationId);
        mailService.sendMail(buildMail(recipientEmail, applicationId, NotificationMail.CREATE));
    }

    public void notifyOnApplicationStatusUpdate(Long applicationId) {
        String recipientEmail = applicationFieldService.getEmailByApplicationId(applicationId);
        mailService.sendMail(buildMail(recipientEmail, applicationId, NotificationMail.UPDATE));
    }

    private Mail buildMail(String recipient, Long applicationId, NotificationMail mail) {
        return Mail.builder()
            .recipient(recipient)
            .subject(mail.getSubject())
            .message(mail.getMessage().formatted(applicationId))
            .build();
    }
}
