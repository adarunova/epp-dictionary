package ru.hse.edu.eppdictionary.mapper;

import org.mapstruct.Mapper;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.util.StringUtils;
import ru.hse.edu.eppdictionary.config.MappersConfig;
import ru.hse.edu.eppdictionary.grpc.PageableRequest;
import ru.hse.edu.eppdictionary.grpc.PageableResponse;
import ru.hse.edu.eppdictionary.grpc.SortRequest;

import java.util.Set;

@Mapper(config = MappersConfig.class)
public interface PageableMapper {

    int DEFAULT_PAGE_SIZE = 50;
    PageRequest DEFAULT_PAGE_REQUEST = PageRequest.of(0, DEFAULT_PAGE_SIZE);

    default PageRequest toPageRequest(Boolean isPageableRequestPresent, PageableRequest pageable,
                                      String defaultSortColumn, Set<String> sortColumns) {
        if (!isPageableRequestPresent) {
            return DEFAULT_PAGE_REQUEST.withSort(Sort.Direction.ASC, defaultSortColumn);
        }

        PageRequest pageRequest = PageRequest.of(
            pageable.getPage(),
            pageable.getPageSize() != 0 ? pageable.getPageSize() : DEFAULT_PAGE_SIZE
        );

        if (!pageable.hasSort()) {
            return pageRequest.withSort(Sort.Direction.ASC, defaultSortColumn);
        }

        SortRequest sortRequest = pageable.getSort();
        validateSortColumns(sortColumns, sortRequest.getColumn());

        return pageRequest.withSort(
            Sort.Direction.valueOf(sortRequest.getOrder().name()),
            StringUtils.hasText(sortRequest.getColumn()) ? sortRequest.getColumn() : defaultSortColumn
        );
    }

    default <T> PageableResponse toPageableResponse(Page<T> page) {
        return PageableResponse.newBuilder()
            .setPage(page.getNumber())
            .setPageSize(page.getNumberOfElements())
            .setTotalElements(page.getTotalElements())
            .setTotalPages(page.getTotalPages())
            .build();
    }

    private void validateSortColumns(Set<String> sortColumns, String column) {
        if (!sortColumns.contains(column)) {
            throw new IllegalArgumentException(
                "Invalid column specified for sorting. Valid columns: %s".formatted(sortColumns)
            );
        }
    }
}
