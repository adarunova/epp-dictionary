package ru.hse.edu.eppdictionary.model.dictionary;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 * Статусы заявки
 */
@Getter
@Setter
@Entity
@Table(name = "epp_status")
public class EppStatus {

    /**
     * Уникальный идентификатор
     */
    @Id
    private int id;

    /**
     * Статус
     */
    @Enumerated(EnumType.STRING)
    private EppStatusType status;
}
