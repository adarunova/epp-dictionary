package ru.hse.edu.eppdictionary.repository.dictionary;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import ru.hse.edu.eppdictionary.model.dictionary.EppStatus;

@Repository
public interface EppStatusRepository extends JpaRepository<EppStatus, Integer> {
}
