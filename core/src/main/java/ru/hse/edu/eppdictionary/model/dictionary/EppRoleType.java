package ru.hse.edu.eppdictionary.model.dictionary;

import lombok.AllArgsConstructor;
import lombok.Getter;

@Getter
@AllArgsConstructor
public enum EppRoleType {

    STUDENT("Студент", "Student"),
    GRADUATE("Внешний заявитель", "External applicant"),
    MANAGER_PROGRAM("Менеджер ОП", "Manager of program"),
    EMPLOYEE("Сотрудник Центра \"Студент\"", "Employee \"Student\" center"),
    UNKNOWN("", "");

    /**
     * Роль на русском языке
     */
    private final String ruName;

    /**
     * Роль на английском языке
     */
    private final String enName;
}
